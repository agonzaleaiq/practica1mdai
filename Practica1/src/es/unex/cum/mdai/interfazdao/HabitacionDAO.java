package es.unex.cum.mdai.interfazdao;

import java.util.List;

import es.unex.cum.mdai.entidades.Centro;
import es.unex.cum.mdai.entidades.Habitacion;

public interface HabitacionDAO {

	Habitacion create(Habitacion habitacion);

	Habitacion read(int id);

	Habitacion update(Habitacion habitacion);

	void delete(Habitacion habitacion);

	List<Habitacion> listarHabitaciones();

	List<Habitacion> listaHabsVacias(Centro c);
	
	List<Habitacion> listaHabsPlanta(int planta);
}
