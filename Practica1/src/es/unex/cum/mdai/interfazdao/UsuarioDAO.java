package es.unex.cum.mdai.interfazdao;

import java.util.List;

import es.unex.cum.mdai.entidades.Centro;
import es.unex.cum.mdai.entidades.Usuario;

public interface UsuarioDAO {

	Usuario create(Usuario user);

	Usuario read(int id);

	Usuario update(Usuario user);

	void delete(Usuario user);
	
	List<Usuario> listarUsuarios();
	
	List<Usuario> listarUsuariosP();
	
	List<Usuario> listarUsuariosF();
	
	List<Usuario> listarUsuariosCentro(Centro c);
}
