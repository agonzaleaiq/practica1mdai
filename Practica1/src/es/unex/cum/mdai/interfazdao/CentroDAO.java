package es.unex.cum.mdai.interfazdao;

import java.util.List;

import es.unex.cum.mdai.entidades.Centro;

public interface CentroDAO {

	Centro create(Centro centro);

	Centro read(int id);

	Centro update(Centro centro);

	void delete(Centro centro);
	
	List<Centro> listarCentros();
}
