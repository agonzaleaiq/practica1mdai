package es.unex.cum.mdai.interfazdao;

import java.util.List;

import es.unex.cum.mdai.entidades.Paciente;
import es.unex.cum.mdai.entidades.Paciente_Tratamiento;
import es.unex.cum.mdai.entidades.Tratamiento;

public interface Paciente_TratamientoDAO {

	Paciente_Tratamiento create(Paciente_Tratamiento tratamiento_paciente);

	Paciente_Tratamiento read(int id);

	Paciente_Tratamiento update(Paciente_Tratamiento tratamiento_paciente);

	void delete(Paciente_Tratamiento tratamiento_paciente);
	
	List<Paciente_Tratamiento> listarPacientesTrat();
	
	List<Paciente_Tratamiento> listarPacientesConUnTrat(Tratamiento t);
	
	List<Paciente_Tratamiento> listarTratamientosPorPaciente(Paciente p);
}
