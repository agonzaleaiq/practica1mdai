package es.unex.cum.mdai.interfazdao;

import java.util.List;

import es.unex.cum.mdai.entidades.Centro;
import es.unex.cum.mdai.entidades.Habitacion;
import es.unex.cum.mdai.entidades.Paciente;

public interface PacienteDAO {

	Paciente create(Paciente paciente);

	Paciente read(int id);

	Paciente update(Paciente paciente);

	void delete(Paciente paciente);
	
	List<Paciente> listarPacientesHab(Habitacion hab, Centro c);
	
	List<Paciente> listarPacientes();
}
