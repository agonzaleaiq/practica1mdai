package es.unex.cum.mdai.interfazdao;

import java.util.List;

import es.unex.cum.mdai.entidades.Tratamiento;

public interface TratamientoDAO {

	Tratamiento create(Tratamiento tratamiento);

	Tratamiento read(int id);

	Tratamiento update(Tratamiento tratamiento);

	void delete(Tratamiento tratamiento);

	List<Tratamiento> listarTratamientosOrdenado();
//	
//	List<Paciente> listarPacientes();
}
