package es.unex.cum.mdai.interfazdao;

import java.util.List;

import es.unex.cum.mdai.entidades.Trabajador;

public interface TrabajadorDAO {

	Trabajador create(Trabajador trabajador);

	Trabajador read(int id);

	Trabajador update(Trabajador trabajador);

	void delete(Trabajador trabajador);

	List<Trabajador> listarTrabajadores();
}
