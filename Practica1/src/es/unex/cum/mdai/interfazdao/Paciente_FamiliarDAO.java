package es.unex.cum.mdai.interfazdao;

import java.util.List;

import es.unex.cum.mdai.entidades.Familiar;
import es.unex.cum.mdai.entidades.Paciente;
import es.unex.cum.mdai.entidades.Paciente_Familiar;

public interface Paciente_FamiliarDAO {

	Paciente_Familiar create(Paciente_Familiar paciente_Familiar);

	Paciente_Familiar read(int id);

	Paciente_Familiar update(Paciente_Familiar paciente_Familiar);

	void delete(Paciente_Familiar paciente_Familiar);

	List<Paciente_Familiar> listarPacientesFamiliares();

	List<Paciente_Familiar> listarPacientesDeUnFamiliar(Familiar f);

	List<Paciente_Familiar> listarFamiliaresPorPaciente(Paciente p);
}
