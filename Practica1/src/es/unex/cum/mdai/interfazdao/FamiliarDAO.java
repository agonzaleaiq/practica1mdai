package es.unex.cum.mdai.interfazdao;

import java.util.List;

import es.unex.cum.mdai.entidades.Familiar;

public interface FamiliarDAO {

	Familiar create(Familiar familiar);

	Familiar read(int id);

	Familiar update(Familiar familiar);

	void delete(Familiar familiar);

	List<Familiar> listarFamiliares();
}
