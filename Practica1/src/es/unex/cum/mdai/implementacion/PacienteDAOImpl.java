package es.unex.cum.mdai.implementacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import es.unex.cum.mdai.entidades.Centro;
import es.unex.cum.mdai.entidades.Habitacion;
import es.unex.cum.mdai.entidades.Paciente;
import es.unex.cum.mdai.interfazdao.PacienteDAO;

public class PacienteDAOImpl implements PacienteDAO {

	protected EntityManager entityManager;

	public PacienteDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Paciente create(Paciente paciente) {
		this.entityManager.persist(paciente);
		return paciente;
	}

	public Paciente read(int id) {
		return this.entityManager.find(Paciente.class, id);
	}

	public Paciente update(Paciente paciente) {
		return this.entityManager.merge(paciente);
	}

	public void delete(Paciente paciente) {
		paciente = this.entityManager.merge(paciente);
		this.entityManager.remove(paciente);

	}
	public List<Paciente> listarPacientes() {
		String queryString = "FROM Paciente";
		Query query = entityManager.createQuery(queryString);
		@SuppressWarnings("unchecked")
		List<Paciente> pacientes = query.getResultList();

		return pacientes;
	}

	public List<Paciente> listarPacientesHab(Habitacion hab, Centro c) {
		String queryString = "FROM Paciente p where p.pacienteHab = :h and p.centroUsers = :c";

		Query query = entityManager.createQuery(queryString);
		query.setParameter("h", hab);
		query.setParameter("c", c);
		@SuppressWarnings("unchecked")
		List<Paciente> pacientes = query.getResultList();

		return pacientes;
	}
}
