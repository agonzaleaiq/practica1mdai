package es.unex.cum.mdai.implementacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import es.unex.cum.mdai.entidades.Trabajador;
import es.unex.cum.mdai.interfazdao.TrabajadorDAO;

public class TrabajadorDAOImpl implements TrabajadorDAO {

	protected EntityManager entityManager;

	public TrabajadorDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Trabajador create(Trabajador trabajador) {
		this.entityManager.persist(trabajador);
		return trabajador;
	}

	public Trabajador read(int id) {
		return this.entityManager.find(Trabajador.class, id);
	}

	public Trabajador update(Trabajador trabajador) {
		return this.entityManager.merge(trabajador);
	}

	public void delete(Trabajador trabajador) {
		trabajador = this.entityManager.merge(trabajador);
		this.entityManager.remove(trabajador);

	}

	public List<Trabajador> listarTrabajadores() {
		String queryString = "FROM Trabajador";
		Query query = entityManager.createQuery(queryString);

		@SuppressWarnings("unchecked")
		List<Trabajador> trab = query.getResultList();

		return trab;
	}
}
