package es.unex.cum.mdai.implementacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import es.unex.cum.mdai.entidades.Paciente;
import es.unex.cum.mdai.entidades.Paciente_Tratamiento;
import es.unex.cum.mdai.entidades.Tratamiento;
import es.unex.cum.mdai.interfazdao.Paciente_TratamientoDAO;

public class Paciente_TratamientoDAOImpl implements Paciente_TratamientoDAO {

	protected EntityManager entityManager;

	public Paciente_TratamientoDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Paciente_Tratamiento create(Paciente_Tratamiento tratamiento_paciente) {
		this.entityManager.persist(tratamiento_paciente);
		return tratamiento_paciente;
	}

	public Paciente_Tratamiento read(int id) {
		return this.entityManager.find(Paciente_Tratamiento.class, id);
	}

	public Paciente_Tratamiento update(Paciente_Tratamiento tratamiento_paciente) {
		return this.entityManager.merge(tratamiento_paciente);
	}

	public void delete(Paciente_Tratamiento tratamiento_paciente) {
		tratamiento_paciente = this.entityManager.merge(tratamiento_paciente);
		this.entityManager.remove(tratamiento_paciente);

	}

	public List<Paciente_Tratamiento> listarPacientesTrat() {
		String queryString = "FROM Paciente_Tratamiento";

		Query query = entityManager.createQuery(queryString);
		@SuppressWarnings("unchecked")
		List<Paciente_Tratamiento> pacientes = query.getResultList();

		return pacientes;
	}

	public List<Paciente_Tratamiento> listarPacientesConUnTrat(Tratamiento t) {
		String queryString = "FROM Paciente_Tratamiento p where p.tratamiento = :t";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("t", t);
		@SuppressWarnings("unchecked")
		List<Paciente_Tratamiento> pacientes = query.getResultList();

		return pacientes;
	}

	public List<Paciente_Tratamiento> listarTratamientosPorPaciente(Paciente p) {
		String queryString = "FROM Paciente_Tratamiento p where p.paciente = :pac";

		Query query = entityManager.createQuery(queryString);
		query.setParameter("pac", p);
		@SuppressWarnings("unchecked")
		List<Paciente_Tratamiento> pacientes = query.getResultList();

		return pacientes;
	}
}
