package es.unex.cum.mdai.implementacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import es.unex.cum.mdai.entidades.Centro;
import es.unex.cum.mdai.interfazdao.CentroDAO;

public class CentroDAOImpl implements CentroDAO {

	protected EntityManager entityManager;

	public CentroDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Centro create(Centro centro) {
		this.entityManager.persist(centro);
		return centro;
	}

	public Centro read(int id) {
		return this.entityManager.find(Centro.class, id);
	}

	public Centro update(Centro centro) {
		return this.entityManager.merge(centro);
	}

	public void delete(Centro centro) {
		centro = this.entityManager.merge(centro);
		this.entityManager.remove(centro);
	}

	public List<Centro> listarCentros() {
		String queryString = "FROM Centro";
		Query query = entityManager.createQuery(queryString);

		@SuppressWarnings("unchecked")
		List<Centro> centro = query.getResultList();
		return centro;
	}
}
