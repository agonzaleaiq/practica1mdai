package es.unex.cum.mdai.implementacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import es.unex.cum.mdai.entidades.Tratamiento;
import es.unex.cum.mdai.interfazdao.TratamientoDAO;

public class TratamientoDAOImpl implements TratamientoDAO {

	protected EntityManager entityManager;

	public TratamientoDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Tratamiento create(Tratamiento tratamiento) {
		this.entityManager.persist(tratamiento);
		return tratamiento;
	}	

	public Tratamiento read(int id) {
		return this.entityManager.find(Tratamiento.class, id);
	}

	public Tratamiento update(Tratamiento tratamiento) {
		return this.entityManager.merge(tratamiento);
	}

	public void delete(Tratamiento tratamiento) {
		tratamiento = this.entityManager.merge(tratamiento);
		this.entityManager.remove(tratamiento);

	}

	public List<Tratamiento> listarTratamientosOrdenado() {
		String queryString = "FROM Tratamiento t order by t.nombre";
		Query query = entityManager.createQuery(queryString);
		@SuppressWarnings("unchecked")
		List<Tratamiento> tramientos = query.getResultList();

		return tramientos;
	}
}
