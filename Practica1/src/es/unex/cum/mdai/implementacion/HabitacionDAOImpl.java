package es.unex.cum.mdai.implementacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import es.unex.cum.mdai.entidades.Centro;
import es.unex.cum.mdai.entidades.Habitacion;
import es.unex.cum.mdai.interfazdao.HabitacionDAO;

public class HabitacionDAOImpl implements HabitacionDAO {

	protected EntityManager entityManager;

	public HabitacionDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Habitacion create(Habitacion habitacion) {
		this.entityManager.persist(habitacion);
		return habitacion;
	}

	public Habitacion read(int id) {
		return this.entityManager.find(Habitacion.class, id);
	}

	public Habitacion update(Habitacion habitacion) {
		return this.entityManager.merge(habitacion);
	}

	public void delete(Habitacion habitacion) {
		habitacion = this.entityManager.merge(habitacion);
		this.entityManager.remove(habitacion);

	}

	public List<Habitacion> listarHabitaciones() {
		String queryString = "FROM Habitacion";
		Query query = entityManager.createQuery(queryString);

		@SuppressWarnings("unchecked")
		List<Habitacion> habitaciones = query.getResultList();

		return habitaciones;
	}

	public List<Habitacion> listaHabsVacias(Centro c) {
		String queryString = "FROM Habitacion h where h.ocupada= :o and centroHab= :c";

		Query query = entityManager.createQuery(queryString);
		query.setParameter("o", 'T');
		query.setParameter("c", c);
		@SuppressWarnings("unchecked")
		List<Habitacion> habitaciones = query.getResultList();

		return habitaciones;
	}

	public List<Habitacion> listaHabsPlanta(int planta) {
		String queryString = "FROM Habitacion h where h.nplanta= :planta";

		Query query = entityManager.createQuery(queryString);
		query.setParameter("planta", planta);
		@SuppressWarnings("unchecked")
		List<Habitacion> habitaciones = query.getResultList();

		return habitaciones;
	}
}
