package es.unex.cum.mdai.implementacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import es.unex.cum.mdai.entidades.Familiar;
import es.unex.cum.mdai.interfazdao.FamiliarDAO;

public class FamiliarDAOImpl implements FamiliarDAO {

	protected EntityManager entityManager;

	public FamiliarDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Familiar create(Familiar familiar) {
		this.entityManager.persist(familiar);
		return familiar;
	}

	public Familiar read(int id) {
		return this.entityManager.find(Familiar.class, id);
	}

	public Familiar update(Familiar familiar) {
		return this.entityManager.merge(familiar);
	}

	public void delete(Familiar familiar) {
		familiar = this.entityManager.merge(familiar);
		this.entityManager.remove(familiar);

	}

	public List<Familiar> listarFamiliares() {
		String queryString = "FROM Familiar";
		Query query = entityManager.createQuery(queryString);
		@SuppressWarnings("unchecked")
		List<Familiar> familiar = query.getResultList();

		return familiar;
	}
}
