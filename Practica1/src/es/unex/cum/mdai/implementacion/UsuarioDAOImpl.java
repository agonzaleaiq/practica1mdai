package es.unex.cum.mdai.implementacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import es.unex.cum.mdai.entidades.Centro;
import es.unex.cum.mdai.entidades.Usuario;
import es.unex.cum.mdai.interfazdao.UsuarioDAO;

public class UsuarioDAOImpl implements UsuarioDAO {

	protected EntityManager entityManager;

	public UsuarioDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Usuario create(Usuario user) {
		this.entityManager.persist(user);
		return user;
	}

	public Usuario read(int id) {
		return this.entityManager.find(Usuario.class, id);
	}

	public Usuario update(Usuario user) {
		return this.entityManager.merge(user);
	}

	public void delete(Usuario user) {
		user = this.entityManager.merge(user);
		this.entityManager.remove(user);

	}

	public List<Usuario> listarUsuarios() {
		String queryString = "FROM Usuario";
		Query query = entityManager.createQuery(queryString);

		@SuppressWarnings("unchecked")
		List<Usuario> users = query.getResultList();

		return users;
	}

	public List<Usuario> listarUsuariosP() {
		String queryString = "FROM Paciente";
		Query query = entityManager.createQuery(queryString);
		@SuppressWarnings("unchecked")
		List<Usuario> users = query.getResultList();

		return users;
	}

	public List<Usuario> listarUsuariosF() {
		String queryString = "FROM Familiar";
		Query query = entityManager.createQuery(queryString);
		@SuppressWarnings("unchecked")
		List<Usuario> users = query.getResultList();

		return users;
	}

	public List<Usuario> listarUsuariosCentro(Centro c) {
		String queryString = "FROM Usuario u where u.centroUsers = :c";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("c", c);
		@SuppressWarnings("unchecked")
		List<Usuario> users = query.getResultList();

		return users;
	}
}
