package es.unex.cum.mdai.implementacion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import es.unex.cum.mdai.entidades.Familiar;
import es.unex.cum.mdai.entidades.Paciente;
import es.unex.cum.mdai.entidades.Paciente_Familiar;
import es.unex.cum.mdai.interfazdao.Paciente_FamiliarDAO;

public class Paciente_FamiliarDAOImpl implements Paciente_FamiliarDAO {

	protected EntityManager entityManager;

	public Paciente_FamiliarDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Paciente_Familiar create(Paciente_Familiar paciente_Familiar) {
		this.entityManager.persist(paciente_Familiar);
		return paciente_Familiar;
	}

	public Paciente_Familiar read(int id) {
		return this.entityManager.find(Paciente_Familiar.class, id);
	}

	public Paciente_Familiar update(Paciente_Familiar paciente_Familiar) {
		return this.entityManager.merge(paciente_Familiar);
	}

	public void delete(Paciente_Familiar paciente_Familiar) {
		paciente_Familiar = this.entityManager.merge(paciente_Familiar);
		this.entityManager.remove(paciente_Familiar);

	}

	public List<Paciente_Familiar> listarPacientesFamiliares() {
		String queryString = "FROM Paciente_Familiar";

		Query query = entityManager.createQuery(queryString);
		@SuppressWarnings("unchecked")
		List<Paciente_Familiar> pacientes = query.getResultList();

		return pacientes;
	}

	public List<Paciente_Familiar> listarPacientesDeUnFamiliar(Familiar f) {
		String queryString = "FROM Paciente_Familiar p where p.familiar = :f";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("f", f);
		@SuppressWarnings("unchecked")
		List<Paciente_Familiar> pacientes = query.getResultList();

		return pacientes;
	}

	public List<Paciente_Familiar> listarFamiliaresPorPaciente(Paciente p) {
		String queryString = "FROM Paciente_Familiar p where p.idpacienteFam = :pac";

		Query query = entityManager.createQuery(queryString);
		query.setParameter("pac", p);
		@SuppressWarnings("unchecked")
		List<Paciente_Familiar> pacientes = query.getResultList();

		return pacientes;
	}
}
