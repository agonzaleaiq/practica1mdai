package es.unex.cum.mdai.entidades;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TRABAJADORES")
public class Trabajador extends Usuario {

	@ManyToOne
	@JoinColumn(name = "centroTrab")
	private Centro centroTrab;

	public Trabajador() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Trabajador(int id_usuario, String nombre, String apellidos, String dni, LocalDate fec_nac) {
		super(id_usuario, nombre, apellidos, dni, fec_nac);
		// TODO Auto-generated constructor stub
	}

	public Centro getCentroTrab() {
		return centroTrab;
	}

	public void setCentroTrab(Centro centroTrab) {
		this.centroTrab = centroTrab;
	}

	@Override
	public String toString() {
		return "Trabajador [centro=" + centroTrab + ", id_usuario=" + id_usuario + ", nombre=" + nombre + ", apellidos="
				+ apellidos + ", dni=" + dni + "]";
	}

}
