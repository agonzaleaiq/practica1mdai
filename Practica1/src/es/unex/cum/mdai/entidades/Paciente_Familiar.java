package es.unex.cum.mdai.entidades;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "Paciente_Familiar")
public class Paciente_Familiar {

	@EmbeddedId
	private Paciente_FamiliarID id;

	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("idpacienteFam")
	private Paciente idpacienteFam;

	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("idfamiliar")
	private Familiar familiar;

	private String relacion;

	public Paciente_Familiar(Paciente_FamiliarID id, Paciente pacienteFam, Familiar familiar, String relacion) {
		super();
		this.id = id;
		this.idpacienteFam = pacienteFam;
		this.familiar = familiar;
		this.relacion = relacion;
	}

	public Paciente_Familiar() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Paciente_FamiliarID getId() {
		return id;
	}

	public void setId(Paciente_FamiliarID id) {
		this.id = id;
	}

	public Paciente getPaciente() {
		return idpacienteFam;
	}

	public void setPaciente(Paciente paciente) {
		this.idpacienteFam = paciente;
	}

	public Familiar getFamiliar() {
		return familiar;
	}

	public void setFamiliar(Familiar familiar) {
		this.familiar = familiar;
	}

	public String getRelacion() {
		return relacion;
	}

	public void setRelacion(String relacion) {
		this.relacion = relacion;
	}

	@Override
	public String toString() {
		return "Paciente_Familiar [id=" + id + ", paciente=" + idpacienteFam + ", familiar=" + familiar + ", relacion="
				+ relacion + "]";
	}

}
