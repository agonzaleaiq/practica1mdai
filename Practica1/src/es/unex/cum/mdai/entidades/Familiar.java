package es.unex.cum.mdai.entidades;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "FAMILIARES")
public class Familiar extends Usuario {

	@OneToMany(mappedBy = "familiar", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Paciente_Familiar> familiares;

	public Familiar() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Familiar(int id_usuario, String nombre, String apellidos, String dni, LocalDate fec_nac) {
		super(id_usuario, nombre, apellidos, dni, fec_nac);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Familiar [id_usuario=" + id_usuario + ", nombre=" + nombre + ", apellidos=" + apellidos + ", dni=" + dni
				+ ", fec_nac=" + fec_nac + "]";
	}

}
