package es.unex.cum.mdai.entidades;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "HABITACIONES")
public class Habitacion {
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private int id_hab;
	private int nhab;
	private int nplanta;
	private char ocupada;

	@ManyToOne
	@JoinColumn(name = "centroHab")
	private Centro centroHab;

	@OneToMany(mappedBy = "pacienteHab")
	private List<Paciente> pacientes;

	public Habitacion(int id_hab, int nhab, int nplanta, char ocupada) {
		super();
		this.id_hab = id_hab;
		this.nhab = nhab;
		this.nplanta = nplanta;
		this.ocupada = ocupada;
	}

	public Habitacion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId_hab() {
		return id_hab;
	}

	public void setId_hab(int id_hab) {
		this.id_hab = id_hab;
	}

	public int getNhab() {
		return nhab;
	}

	public void setNhab(int nhab) {
		this.nhab = nhab;
	}

	public int getNplanta() {
		return nplanta;
	}

	public void setNplanta(int nplanta) {
		this.nplanta = nplanta;
	}

	public char getOcupada() {
		return ocupada;
	}

	public void setOcupada(char ocupada) {
		this.ocupada = ocupada;
	}

	public Centro getCentroHab() {
		return centroHab;
	}

	public void setCentroHab(Centro centroHab) {
		this.centroHab = centroHab;
	}

	public List<Paciente> getPacientes() {
		return pacientes;
	}

	public void setPacientes(List<Paciente> pacientes) {
		this.pacientes = pacientes;
	}

	@Override
	public String toString() {
		return "Habitaciones [id_hab=" + id_hab + ", nhab=" + nhab + ", nplanta=" + nplanta + ", ocupada=" + ocupada
				+ ", centroHab=" + centroHab + "]";
	}

}
