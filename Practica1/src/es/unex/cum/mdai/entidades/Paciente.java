package es.unex.cum.mdai.entidades;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PACIENTES")
public class Paciente extends Usuario {

	@ManyToOne
	@JoinColumn(name = "pacienteHab")
	private Habitacion pacienteHab;

	@OneToMany(mappedBy = "paciente", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Paciente_Tratamiento> pacientes;

	@OneToMany(mappedBy = "idpacienteFam", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Paciente_Familiar> idpacienteFam;

	public Paciente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Paciente(int id_usuario, String nombre, String apellidos, String dni, LocalDate fec_nac) {
		super(id_usuario, nombre, apellidos, dni, fec_nac);
		// TODO Auto-generated constructor stub
	}

	public Habitacion getPacienteHab() {
		return pacienteHab;
	}

	public void setPacienteHab(Habitacion pacienteHab) {
		this.pacienteHab = pacienteHab;
	}

	public List<Paciente_Tratamiento> getPacientes() {
		return pacientes;
	}

	public void setPacientes(List<Paciente_Tratamiento> pacientes) {
		this.pacientes = pacientes;
	}

	public List<Paciente_Familiar> getPacientesFam() {
		return idpacienteFam;
	}

	public void setPacientesFam(List<Paciente_Familiar> pacientesFam) {
		this.idpacienteFam = pacientesFam;
	}

	@Override
	public String toString() {
		return "Paciente [pacienteHab=" + pacienteHab + ", id_usuario=" + id_usuario + ", nombre=" + nombre
				+ ", apellidos=" + apellidos + ", dni=" + dni + ", fec_nac=" + fec_nac + ", PacienteHab="
				+ getPacienteHab() + ", Centro=" + getCentroUsers() + "]";
	}

}
