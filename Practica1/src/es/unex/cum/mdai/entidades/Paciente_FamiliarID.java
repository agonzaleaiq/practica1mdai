package es.unex.cum.mdai.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Paciente_FamiliarID implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "idpacienteFam")
	private int idpacienteFam;

	@Column(name = "idfamiliar")
	private int idfamiliar;

	public Paciente_FamiliarID() {
		super();
	}

	public Paciente_FamiliarID(int idpaciente, int idfamiliar) {
		super();
		this.idpacienteFam = idpaciente;
		this.idfamiliar = idfamiliar;
	}

	public int getIdpaciente() {
		return idpacienteFam;
	}

	public void setIdpaciente(int idpaciente) {
		this.idpacienteFam = idpaciente;
	}

	public int getIdFamiliar() {
		return idfamiliar;
	}

	public void setIdFamiliar(int idFamiliar) {
		this.idfamiliar = idFamiliar;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idfamiliar;
		result = prime * result + idpacienteFam;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Paciente_FamiliarID other = (Paciente_FamiliarID) obj;
		if (idfamiliar != other.idfamiliar)
			return false;
		if (idpacienteFam != other.idpacienteFam)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Paciente_FamiliarID [idpaciente=" + idpacienteFam + ", idFamiliar=" + idfamiliar + "]";
	}

}
