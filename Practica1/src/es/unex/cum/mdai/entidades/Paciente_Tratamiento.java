package es.unex.cum.mdai.entidades;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "Paciente_tratamiento")
public class Paciente_Tratamiento {

	@EmbeddedId
	private Paciente_tratamientoID id;

	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("idpaciente")
	private Paciente paciente;

	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("idtratamiento")
	private Tratamiento tratamiento;
	
	private float dosis;
	
	private int veces_dias;
	
	private int cuantos_dias;

	public Paciente_tratamientoID getId() {
		return id;
	}

	public void setId(Paciente_tratamientoID id) {
		this.id = id;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Tratamiento getTratamiento() {
		return tratamiento;
	}

	public void setTratamiento(Tratamiento tratamiento) {
		this.tratamiento = tratamiento;
	}

	public float getDosis() {
		return dosis;
	}

	public void setDosis(float dosis) {
		this.dosis = dosis;
	}

	public int getVeces_dias() {
		return veces_dias;
	}

	public void setVeces_dias(int veces_dias) {
		this.veces_dias = veces_dias;
	}

	public int getCuantos_dias() {
		return cuantos_dias;
	}

	public void setCuantos_dias(int cuantos_dias) {
		this.cuantos_dias = cuantos_dias;
	}

	@Override
	public String toString() {
		return "Paciente_Tratamiento [paciente=" + paciente + ", tratamiento=" + tratamiento + ", dosis="
				+ dosis + ", veces_dias=" + veces_dias + ", cuantos_dias=" + cuantos_dias + "]";
	}
	
	

}
