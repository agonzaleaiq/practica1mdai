package es.unex.cum.mdai.entidades;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "USUARIOS")
public abstract class Usuario {
	@Id
	@GeneratedValue(generator = "increment", strategy = GenerationType.IDENTITY)
	@GenericGenerator(name = "increment", strategy = "increment")
	protected int id_usuario;
	protected String nombre;
	protected String apellidos;
	protected String dni;
	protected LocalDate fec_nac;

	@ManyToOne
	@JoinColumn(name = "centroUsers")
	private Centro centroUsers;

	public Usuario(int id_usuario, String nombre, String apellidos, String dni, LocalDate fec_nac) {
		super();
		this.id_usuario = id_usuario;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.dni = dni;
		this.fec_nac = fec_nac;
	}

	public Usuario() {

	}

	public int getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public LocalDate getFec_nac() {
		return fec_nac;
	}

	public void setFec_nac(LocalDate fec_nac) {
		this.fec_nac = fec_nac;
	}

	public Centro getCentroUsers() {
		return centroUsers;
	}

	public void setCentroUsers(Centro centroUsers) {
		this.centroUsers = centroUsers;
	}

	@Override
	public String toString() {
		return "Usuarios [id_usuario=" + id_usuario + ", nombre=" + nombre + ", apellidos=" + apellidos + ", dni=" + dni
				+ ", fec_nac=" + fec_nac + "]";
	}
}
