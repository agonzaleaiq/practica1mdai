package es.unex.cum.mdai.entidades;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "CENTROS")
public class Centro {

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private int id_centro;
	private String nombre;
	private String direccion;
	private int nhabs;
	private int nPlantas;
	private int ntlf;

	@OneToMany(mappedBy = "centroTrab")
	private List<Trabajador> trabajadores;

	@OneToMany(mappedBy = "centroHab")
	private List<Habitacion> habitaciones;
	
	@OneToMany(mappedBy = "centroUsers")
	private List<Usuario> usuarios;

	public Centro(int id_centro, String nombre, String direccion, int nhabs, int nPlantas, int ntlf) {
		super();
		this.id_centro = id_centro;
		this.nombre = nombre;
		this.direccion = direccion;
		this.nhabs = nhabs;
		this.nPlantas = nPlantas;
		this.ntlf = ntlf;
	}

	public Centro() {

	}

	public int getId_centro() {
		return id_centro;
	}

	public void setId_centro(int id_centro) {
		this.id_centro = id_centro;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getNhabs() {
		return nhabs;
	}

	public void setNhabs(int nhabs) {
		this.nhabs = nhabs;
	}

	public int getnPlantas() {
		return nPlantas;
	}

	public void setnPlantas(int nPlantas) {
		this.nPlantas = nPlantas;
	}

	public int getNtlf() {
		return ntlf;
	}

	public void setNtlf(int ntlf) {
		this.ntlf = ntlf;
	}

	public List<Habitacion> getHabitaciones() {
		return habitaciones;
	}

	public void setHabitaciones(List<Habitacion> habitaciones) {
		this.habitaciones = habitaciones;
	}

	public List<Trabajador> getTrabajadores() {
		return trabajadores;
	}

	public void setTrabajadores(List<Trabajador> trabajadores) {
		this.trabajadores = trabajadores;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	@Override
	public String toString() {
		return "Centro [id_centro=" + id_centro + ", nombre=" + nombre + ", direccion=" + direccion + ", nhabs=" + nhabs
				+ ", nPlantas=" + nPlantas + ", ntlf=" + ntlf + "]";
	}




	

}
