package es.unex.cum.mdai.entidades;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "TRATAMIENTOS")
//@NaturalIdCache
public class Tratamiento {

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private int id_tratamiento;
	private String nombre;

	@OneToMany(mappedBy = "tratamiento", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Paciente_Tratamiento> posts;

	public int getId_tratamiento() {
		return id_tratamiento;
	}

	public void setId_tratamiento(int id_tratamiento) {
		this.id_tratamiento = id_tratamiento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Paciente_Tratamiento> getPosts() {
		return posts;
	}

	public void setPosts(List<Paciente_Tratamiento> posts) {
		this.posts = posts;
	}

	@Override
	public String toString() {
		return "Tratamiento [id_tratamiento=" + id_tratamiento + ", nombre=" + nombre + "]";
	}

}
