package es.unex.cum.mdai.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Paciente_tratamientoID implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "idpaciente")
	private int idpaciente;
	
	@Column(name = "idtratamiento")
	private int idtratamiento;

	public Paciente_tratamientoID() {
		super();
	}

	public Paciente_tratamientoID(int idpaciente, int idtratamiento) {
		super();
		this.idpaciente = idpaciente;
		this.idtratamiento = idtratamiento;
	}

	public int getIdpaciente() {
		return idpaciente;
	}

	public void setIdpaciente(int idpaciente) {
		this.idpaciente = idpaciente;
	}

	public int getIdtratamiento() {
		return idtratamiento;
	}

	public void setIdtratamiento(int idtratamiento) {
		this.idtratamiento = idtratamiento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idpaciente;
		result = prime * result + idtratamiento;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Paciente_tratamientoID other = (Paciente_tratamientoID) obj;
		if (idpaciente != other.idpaciente)
			return false;
		if (idtratamiento != other.idtratamiento)
			return false;
		return true;
	}
}
