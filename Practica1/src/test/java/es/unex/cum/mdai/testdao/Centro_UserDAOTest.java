package es.unex.cum.mdai.testdao;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import es.unex.cum.mdai.entidades.Centro;
import es.unex.cum.mdai.entidades.Familiar;
import es.unex.cum.mdai.entidades.Paciente;
import es.unex.cum.mdai.entidades.Trabajador;
import es.unex.cum.mdai.entidades.Usuario;
import es.unex.cum.mdai.implementacion.CentroDAOImpl;
import es.unex.cum.mdai.implementacion.UsuarioDAOImpl;
import es.unex.cum.mdai.interfazdao.CentroDAO;
import es.unex.cum.mdai.interfazdao.UsuarioDAO;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Centro_UserDAOTest {

	private static EntityManagerFactory emf;
	private EntityManager em;

	@BeforeClass
	public static void createEntityManagerFactory() {
		emf = Persistence.createEntityManagerFactory("es.unex.cum.persistenceunit");
	}

	@AfterClass
	public static void closeEntityManagerFactory() {
		emf.close();
	}

	@Before
	public void beginTransaction() {
		em = emf.createEntityManager();
		em.getTransaction().begin();
	}

	@After
	public void rollbackTransaction() {
		// commit
		if (em.getTransaction().isActive()) {
			em.getTransaction().commit();
		}
		if (em.isOpen()) {
			em.close();
		}
	}

	@Test
	public void addCentros() {
		System.out.println("Creando los Centros....");
		CentroDAO centroDAO = new CentroDAOImpl(em);

		Centro centro = new Centro();
		centro.setNombre("Las Encinas");
		centro.setDireccion("C/ Misericordia");
		centro.setNhabs(123);
		centro.setnPlantas(3);
		centro.setNtlf(924481200);
		centroDAO.create(centro);

		Centro centro2 = new Centro();
		centro2.setNombre("Los Olmos");
		centro2.setDireccion("C/ Maria Auxiliadora");
		centro2.setNhabs(215);
		centro2.setnPlantas(4);
		centro2.setNtlf(923476501);
		centroDAO.create(centro2);

		System.out.println("Listado de centros aniadidos:");
		List<Centro> centros = centroDAO.listarCentros();

		for (int i = 0; i < centros.size(); i++) {
			System.out.println(centros.get(i).toString());
		}
	}

	@Test
	public void addUsuarios() {
		System.out.println("Creando los Usuarios....");
		UsuarioDAO userDAO = new UsuarioDAOImpl(em);
		CentroDAO centroDAO = new CentroDAOImpl(em);

		Paciente user = new Paciente();
		user.setNombre("Aida");
		user.setApellidos("Gonzalez");
		user.setDni("72652409c");
		user.setFec_nac(LocalDate.of(1985, Month.JANUARY, 1));
		user.setCentroUsers(centroDAO.read(1));
		userDAO.create(user);

		Familiar user2 = new Familiar();
		user2.setNombre("Laura");
		user2.setApellidos("Lunar");
		user2.setDni("12345649M");
		user2.setFec_nac(LocalDate.of(1940, Month.MARCH, 5));
		user2.setCentroUsers(centroDAO.read(2));
		userDAO.create(user2);

		Trabajador user3 = new Trabajador();
		user3.setNombre("Juanjo");
		user3.setApellidos("Gil");
		user3.setDni("87436757b");
		user3.setFec_nac(LocalDate.of(1925, Month.JULY, 20));
		user3.setCentroUsers(centroDAO.read(1));
		userDAO.create(user3);

		System.out.println("Lista de usuarios:");
		List<Usuario> listaUsers = userDAO.listarUsuarios();

		for (int i = 0; i < listaUsers.size(); i++) {
			System.out.println(listaUsers.get(i).toString());
		}

		System.out.println("Lista de usuarios tipo familiares:");
		List<Usuario> listaUsersf = userDAO.listarUsuariosF();

		for (int i = 0; i < listaUsersf.size(); i++) {
			System.out.println(listaUsersf.get(i).toString());
		}
		System.out.println("Lista de usuarios tipo pacientes:");
		List<Usuario> listaUsersp = userDAO.listarUsuariosP();

		for (int i = 0; i < listaUsersp.size(); i++) {
			System.out.println(listaUsersp.get(i).toString());
		}
		System.out.println("Lista de usuarios del centro 1:");
		List<Usuario> listaUsersPorCentro = userDAO.listarUsuariosCentro(centroDAO.read(1));

		for (int i = 0; i < listaUsersPorCentro.size(); i++) {
			System.out.println(listaUsersPorCentro.get(i).toString());
		}
	}

}
