package es.unex.cum.mdai.testdao;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import es.unex.cum.mdai.entidades.Familiar;
import es.unex.cum.mdai.entidades.Paciente;
import es.unex.cum.mdai.entidades.Paciente_Familiar;
import es.unex.cum.mdai.entidades.Paciente_FamiliarID;
import es.unex.cum.mdai.implementacion.CentroDAOImpl;
import es.unex.cum.mdai.implementacion.FamiliarDAOImpl;
import es.unex.cum.mdai.implementacion.HabitacionDAOImpl;
import es.unex.cum.mdai.implementacion.PacienteDAOImpl;
import es.unex.cum.mdai.implementacion.Paciente_FamiliarDAOImpl;
import es.unex.cum.mdai.interfazdao.CentroDAO;
import es.unex.cum.mdai.interfazdao.FamiliarDAO;
import es.unex.cum.mdai.interfazdao.HabitacionDAO;
import es.unex.cum.mdai.interfazdao.PacienteDAO;
import es.unex.cum.mdai.interfazdao.Paciente_FamiliarDAO;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Paciente_FamiliarDAOTest {

	private static EntityManagerFactory emf;
	private EntityManager em;

	@BeforeClass
	public static void createEntityManagerFactory() {
		emf = Persistence.createEntityManagerFactory("es.unex.cum.persistenceunit");
	}

	@AfterClass
	public static void closeEntityManagerFactory() {
		emf.close();
	}

	@Before
	public void beginTransaction() {
		em = emf.createEntityManager();
		em.getTransaction().begin();
	}

	@After
	public void rollbackTransaction() {
		if (em.getTransaction().isActive()) {
			em.getTransaction().commit();
		}
		if (em.isOpen()) {
			em.close();
		}
	}

	@Test
	public void addPacientes() {
		System.out.println("Creando los Pacientes....");
		PacienteDAO pacienteDAO = new PacienteDAOImpl(em);
		CentroDAO centroDAO = new CentroDAOImpl(em);
		HabitacionDAO habitacionDAO = new HabitacionDAOImpl(em);

		Paciente paciente = new Paciente();
		paciente.setNombre("Clara");
		paciente.setApellidos("Dominguez");
		paciente.setDni("54136782E");
		paciente.setCentroUsers(centroDAO.read(1));
		paciente.setFec_nac(LocalDate.of(1995, Month.APRIL, 10));
		paciente.setPacienteHab(habitacionDAO.read(1));
		pacienteDAO.create(paciente);

		Paciente paciente2 = new Paciente();
		paciente2.setNombre("Marcos");
		paciente2.setApellidos("Jimenez");
		paciente2.setDni("89256734g");
		paciente2.setCentroUsers(centroDAO.read(2));
		paciente2.setFec_nac(LocalDate.of(1990, Month.MARCH, 30));
		paciente2.setPacienteHab(habitacionDAO.read(1));
		pacienteDAO.create(paciente2);

		System.out.println("Lista de usuarios tipo pacientes:");
		List<Paciente> listaUsersp = pacienteDAO.listarPacientes();

		for (int i = 0; i < listaUsersp.size(); i++) {
			System.out.println(listaUsersp.get(i).toString());
		}
	}

	@Test
	public void addFamiliares() {
		System.out.println("Creando los Familiares....");
		FamiliarDAO familiarDAO = new FamiliarDAOImpl(em);
		CentroDAO centroDAO = new CentroDAOImpl(em);

		Familiar familiar = new Familiar();
		familiar.setNombre("Aida");
		familiar.setApellidos("Gonzalez");
		familiar.setDni("76362918u");
		familiar.setCentroUsers(centroDAO.read(1));
		familiar.setFec_nac(LocalDate.of(1995, Month.APRIL, 10));
		familiarDAO.create(familiar);

		Familiar familiar2 = new Familiar();
		familiar2.setNombre("Laura");
		familiar2.setApellidos("Jimenez");
		familiar2.setDni("28193746c");
		familiar2.setCentroUsers(centroDAO.read(1));
		familiar2.setFec_nac(LocalDate.of(1995, Month.APRIL, 10));
		familiarDAO.create(familiar2);

		System.out.println("Lista de usuarios tipo familiares:");
		List<Familiar> listarFamiliares = familiarDAO.listarFamiliares();

		for (int i = 0; i < listarFamiliares.size(); i++) {
			System.out.println(listarFamiliares.get(i).toString());
		}
	}

	@Test
	public void addPacientesFamiliares() {
		System.out.println("Creando las relaciones entre pacientes y familiares...");
		Paciente_FamiliarDAO paciente_FamiliarDAO = new Paciente_FamiliarDAOImpl(em);
		PacienteDAO pacienteDAO = new PacienteDAOImpl(em);
		FamiliarDAO familiarDAO = new FamiliarDAOImpl(em);

		Paciente_FamiliarID pac_famID1 = new Paciente_FamiliarID();
		Paciente_Familiar pac_fam1 = new Paciente_Familiar();
		pac_fam1.setId(pac_famID1);
		pac_fam1.setPaciente(pacienteDAO.read(3));
		pac_fam1.setFamiliar(familiarDAO.read(1));
		pac_fam1.setRelacion("Hija");
		paciente_FamiliarDAO.create(pac_fam1);

		Paciente_FamiliarID pac_famID2 = new Paciente_FamiliarID();
		Paciente_Familiar pac_fam2 = new Paciente_Familiar();
		pac_fam2.setId(pac_famID2);
		pac_fam2.setPaciente(pacienteDAO.read(3));
		pac_fam2.setFamiliar(familiarDAO.read(2));
		pac_fam2.setRelacion("Nieta");
		paciente_FamiliarDAO.create(pac_fam2);

		Paciente_FamiliarID pac_famID3 = new Paciente_FamiliarID();
		Paciente_Familiar pac_fam3 = new Paciente_Familiar();
		pac_fam3.setId(pac_famID3);
		pac_fam3.setPaciente(pacienteDAO.read(4));
		pac_fam3.setFamiliar(familiarDAO.read(2));
		pac_fam3.setRelacion("Sobrino");
		paciente_FamiliarDAO.create(pac_fam3);

		System.out.println("Lista de los pacientes y de que familiares tienen: ");

		List<Paciente_Familiar> listaPacientesFam = paciente_FamiliarDAO.listarPacientesFamiliares();
		for (int i = 0; i < listaPacientesFam.size(); i++) {
			System.out.println(listaPacientesFam.get(i).toString());

		}

		System.out.println("Lista de los pacientes que tienen el familiar 2: ");

		List<Paciente_Familiar> listaPacientesporFamiliar = paciente_FamiliarDAO
				.listarPacientesDeUnFamiliar(familiarDAO.read(2));
		for (int i = 0; i < listaPacientesporFamiliar.size(); i++) {
			System.out.println(listaPacientesporFamiliar.get(i).toString());

		}

		System.out.println("Lista de los familaires que tiene el paciente 1 (que es el id 3 de usuarios): ");

		List<Paciente_Familiar> listaFamiliaresPac = paciente_FamiliarDAO
				.listarFamiliaresPorPaciente(pacienteDAO.read(3));
		for (int i = 0; i < listaFamiliaresPac.size(); i++) {
			System.out.println(listaFamiliaresPac.get(i).toString());

		}

	}
}
