package es.unex.cum.mdai.testdao;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import es.unex.cum.mdai.entidades.Paciente;
import es.unex.cum.mdai.entidades.Paciente_Tratamiento;
import es.unex.cum.mdai.entidades.Paciente_tratamientoID;
import es.unex.cum.mdai.entidades.Tratamiento;
import es.unex.cum.mdai.implementacion.CentroDAOImpl;
import es.unex.cum.mdai.implementacion.HabitacionDAOImpl;
import es.unex.cum.mdai.implementacion.PacienteDAOImpl;
import es.unex.cum.mdai.implementacion.Paciente_TratamientoDAOImpl;
import es.unex.cum.mdai.implementacion.TratamientoDAOImpl;
import es.unex.cum.mdai.interfazdao.CentroDAO;
import es.unex.cum.mdai.interfazdao.HabitacionDAO;
import es.unex.cum.mdai.interfazdao.PacienteDAO;
import es.unex.cum.mdai.interfazdao.Paciente_TratamientoDAO;
import es.unex.cum.mdai.interfazdao.TratamientoDAO;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Paciente_TratamientoDAOTest {

	private static EntityManagerFactory emf;
	private EntityManager em;

	@BeforeClass
	public static void createEntityManagerFactory() {
		emf = Persistence.createEntityManagerFactory("es.unex.cum.persistenceunit");
	}

	@AfterClass
	public static void closeEntityManagerFactory() {
		emf.close();
	}

	@Before
	public void beginTransaction() {
		em = emf.createEntityManager();
		em.getTransaction().begin();
	}

	@After
	public void rollbackTransaction() {
		if (em.getTransaction().isActive()) {
			em.getTransaction().commit();
		}
		if (em.isOpen()) {
			em.close();
		}
	}

	@Test
	public void addPacientes() {
		System.out.println("Creando los Pacientes....");
		PacienteDAO pacienteDAO = new PacienteDAOImpl(em);
		CentroDAO centroDAO = new CentroDAOImpl(em);
		HabitacionDAO habitacionDAO = new HabitacionDAOImpl(em);

		Paciente paciente = new Paciente();
		paciente.setNombre("Clara");
		paciente.setApellidos("Dominguez");
		paciente.setDni("54136782E");
		paciente.setCentroUsers(centroDAO.read(1));
		paciente.setFec_nac(LocalDate.of(1995, Month.APRIL, 10));
		paciente.setPacienteHab(habitacionDAO.read(1));
		pacienteDAO.create(paciente);

		Paciente paciente2 = new Paciente();
		paciente2.setNombre("Marcos");
		paciente2.setApellidos("Jimenez");
		paciente2.setDni("89256734g");
		paciente2.setCentroUsers(centroDAO.read(2));
		paciente2.setFec_nac(LocalDate.of(1990, Month.MARCH, 30));
		paciente2.setPacienteHab(habitacionDAO.read(1));
		pacienteDAO.create(paciente2);

		System.out.println("Lista de usuarios tipo pacientes:");
		List<Paciente> listaUsersp = pacienteDAO.listarPacientes();

		for (int i = 0; i < listaUsersp.size(); i++) {
			System.out.println(listaUsersp.get(i).toString());
		}
	}

	@Test
	public void addTratamiento() {
		System.out.println("Creando los Tratamientos....");
		TratamientoDAO tratamientoDAO = new TratamientoDAOImpl(em);

		Tratamiento tratamiento1 = new Tratamiento();
		tratamiento1.setNombre("Ibuprofeno");
		tratamientoDAO.create(tratamiento1);

		Tratamiento tratamiento2 = new Tratamiento();
		tratamiento2.setNombre("Paracetamol");
		tratamientoDAO.create(tratamiento2);

		Tratamiento tratamiento3 = new Tratamiento();
		tratamiento3.setNombre("Masajes");
		tratamientoDAO.create(tratamiento3);

		Tratamiento tratamiento4 = new Tratamiento();
		tratamiento4.setNombre("Rehabilitacion");
		tratamientoDAO.create(tratamiento4);

		Tratamiento tratamiento5 = new Tratamiento();
		tratamiento5.setNombre("Naproxeno");
		tratamientoDAO.create(tratamiento5);

		System.out.println("Lista de tratamientos en orden alfabetico: ");
		List<Tratamiento> listatratamientos = tratamientoDAO.listarTratamientosOrdenado();

		for (int i = 0; i < listatratamientos.size(); i++) {
			System.out.println(listatratamientos.get(i).toString());
		}
	}

	@Test
	public void addTratamientosDelPaciente() {
		System.out.println("Creando los Tratamientos de cada paciente....");
		Paciente_TratamientoDAO paciente_TratamientoDAO = new Paciente_TratamientoDAOImpl(em);
		PacienteDAO pacienteDAO = new PacienteDAOImpl(em);
		TratamientoDAO tratamientoDAO = new TratamientoDAOImpl(em);

		Paciente_tratamientoID pac_tratID1 = new Paciente_tratamientoID();
		Paciente_Tratamiento pac_trat1 = new Paciente_Tratamiento();
		pac_trat1.setId(pac_tratID1);
		pac_trat1.setPaciente(pacienteDAO.read(1));
		pac_trat1.setTratamiento(tratamientoDAO.read(1));
		pac_trat1.setCuantos_dias(7);
		pac_trat1.setDosis(100);
		pac_trat1.setVeces_dias(3);
		paciente_TratamientoDAO.create(pac_trat1);

		Paciente_tratamientoID pac_tratID2 = new Paciente_tratamientoID();
		Paciente_Tratamiento pac_trat2 = new Paciente_Tratamiento();
		pac_trat2.setId(pac_tratID2);
		pac_trat2.setPaciente(pacienteDAO.read(2));
		pac_trat2.setTratamiento(tratamientoDAO.read(2));
		pac_trat2.setCuantos_dias(7);
		pac_trat2.setDosis(100);
		pac_trat2.setVeces_dias(3);
		paciente_TratamientoDAO.create(pac_trat2);

		Paciente_tratamientoID pac_tratID3 = new Paciente_tratamientoID();
		Paciente_Tratamiento pac_trat3 = new Paciente_Tratamiento();
		pac_trat3.setId(pac_tratID3);
		pac_trat3.setPaciente(pacienteDAO.read(1));
		pac_trat3.setTratamiento(tratamientoDAO.read(3));
		pac_trat3.setCuantos_dias(10);
		pac_trat3.setDosis(100);
		pac_trat3.setVeces_dias(3);
		paciente_TratamientoDAO.create(pac_trat3);

		Paciente_tratamientoID pac_tratID4 = new Paciente_tratamientoID();
		Paciente_Tratamiento pac_trat4 = new Paciente_Tratamiento();
		pac_trat4.setId(pac_tratID4);
		pac_trat4.setPaciente(pacienteDAO.read(2));
		pac_trat4.setTratamiento(tratamientoDAO.read(4));
		pac_trat4.setCuantos_dias(30);
		pac_trat4.setVeces_dias(1);
		paciente_TratamientoDAO.create(pac_trat4);

		Paciente_tratamientoID pac_tratID5 = new Paciente_tratamientoID();
		Paciente_Tratamiento pac_trat5 = new Paciente_Tratamiento();
		pac_trat5.setId(pac_tratID5);
		pac_trat5.setPaciente(pacienteDAO.read(2));
		pac_trat5.setTratamiento(tratamientoDAO.read(5));
		pac_trat5.setCuantos_dias(30);
		pac_trat5.setVeces_dias(1);
		paciente_TratamientoDAO.create(pac_trat5);

		System.out.println("Lista de los pacientes y de que tratamientos tienen: ");

		List<Paciente_Tratamiento> listaPacientesTrat = paciente_TratamientoDAO.listarPacientesTrat();
		for (int i = 0; i < listaPacientesTrat.size(); i++) {
			System.out.println(listaPacientesTrat.get(i).toString());

		}

		System.out.println("Lista de los pacientes que tienen el tratamiento 1: ");

		List<Paciente_Tratamiento> listaPacientesporTr = paciente_TratamientoDAO
				.listarPacientesConUnTrat(tratamientoDAO.read(1));
		for (int i = 0; i < listaPacientesporTr.size(); i++) {
			System.out.println(listaPacientesporTr.get(i).toString());

		}

		System.out.println("Lista de los tratamientos que tiene el paciente 1.: ");

		List<Paciente_Tratamiento> listaTratamientoPac = paciente_TratamientoDAO
				.listarTratamientosPorPaciente(pacienteDAO.read(1));
		for (int i = 0; i < listaTratamientoPac.size(); i++) {
			System.out.println(listaTratamientoPac.get(i).toString());

		}

	}
}
