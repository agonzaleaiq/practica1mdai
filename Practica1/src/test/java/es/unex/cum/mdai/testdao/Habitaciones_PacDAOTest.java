package es.unex.cum.mdai.testdao;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import es.unex.cum.mdai.entidades.Centro;
import es.unex.cum.mdai.entidades.Habitacion;
import es.unex.cum.mdai.entidades.Paciente;
import es.unex.cum.mdai.implementacion.CentroDAOImpl;
import es.unex.cum.mdai.implementacion.HabitacionDAOImpl;
import es.unex.cum.mdai.implementacion.PacienteDAOImpl;
import es.unex.cum.mdai.interfazdao.CentroDAO;
import es.unex.cum.mdai.interfazdao.HabitacionDAO;
import es.unex.cum.mdai.interfazdao.PacienteDAO;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Habitaciones_PacDAOTest {

	private static EntityManagerFactory emf;
	private EntityManager em;

	@BeforeClass
	public static void createEntityManagerFactory() {
		emf = Persistence.createEntityManagerFactory("es.unex.cum.persistenceunit");
	}

	@AfterClass
	public static void closeEntityManagerFactory() {
		emf.close();
	}

	@Before
	public void beginTransaction() {
		em = emf.createEntityManager();
		em.getTransaction().begin();
	}

	@After
	public void rollbackTransaction() {
		// commit
		if (em.getTransaction().isActive()) {
			em.getTransaction().commit();
		}
		if (em.isOpen()) {
			em.close();
		}
	}

	@Test
	public void addCentros() {
		System.out.println("Creando los Centros....");
		CentroDAO centroDAO = new CentroDAOImpl(em);

		Centro centro = new Centro();
		centro.setNombre("Las Encinas");
		centro.setDireccion("C/ Misericordia");
		centro.setNhabs(123);
		centro.setnPlantas(3);
		centro.setNtlf(924481200);
		centroDAO.create(centro);

		Centro centro2 = new Centro();
		centro2.setNombre("Los Olmos");
		centro2.setDireccion("C/ Maria Auxiliadora");
		centro2.setNhabs(215);
		centro2.setnPlantas(4);
		centro2.setNtlf(923476501);
		centroDAO.create(centro2);

		System.out.println("Listado de centros aniadidos:");
		List<Centro> centros = centroDAO.listarCentros();

		for (int i = 0; i < centros.size(); i++) {
			System.out.println(centros.get(i).toString());
		}
	}

	@Test
	public void addHabitaciones() {
		System.out.println("Creando las Habitaciones....");
		HabitacionDAO habitacionDAO = new HabitacionDAOImpl(em);
		CentroDAO centroDAO = new CentroDAOImpl(em);
		// CentroDAO centroDAO = new CentroDAOImpl(em);

		Habitacion habitacion = new Habitacion();
		habitacion.setNhab(12);
		habitacion.setNplanta(1);
		habitacion.setOcupada('T');
		habitacion.setCentroHab(centroDAO.read(1));
		habitacionDAO.create(habitacion);

		Habitacion habitacion2 = new Habitacion();
		habitacion2.setNhab(11);
		habitacion2.setNplanta(1);
		habitacion2.setOcupada('F');
		habitacion2.setCentroHab(centroDAO.read(1));
		habitacionDAO.create(habitacion2);

		Habitacion habitacion3 = new Habitacion();
		habitacion3.setNhab(24);
		habitacion3.setNplanta(2);
		habitacion3.setOcupada('F');
		habitacion3.setCentroHab(centroDAO.read(1));
		habitacionDAO.create(habitacion3);

		Habitacion habitacion4 = new Habitacion();
		habitacion4.setNhab(23);
		habitacion4.setNplanta(2);
		habitacion4.setOcupada('T');
		habitacion4.setCentroHab(centroDAO.read(1));
		habitacionDAO.create(habitacion4);

		System.out.println("Listado de habitaciones aniadidas:");
		List<Habitacion> listaHabs = habitacionDAO.listarHabitaciones();
		for (int i = 0; i < listaHabs.size(); i++) {
			System.out.println(listaHabs.get(i).toString());
		}
	}

	@Test
	public void addPacientes() {
		System.out.println("Creando los Pacientes....");
		PacienteDAO pacienteDAO = new PacienteDAOImpl(em);
		CentroDAO centroDAO = new CentroDAOImpl(em);
		HabitacionDAO habitacionDAO = new HabitacionDAOImpl(em);

		Paciente paciente = new Paciente();
		paciente.setNombre("Clara");
		paciente.setApellidos("Dominguez");
		paciente.setDni("54136782E");
		paciente.setCentroUsers(centroDAO.read(1));
		paciente.setFec_nac(LocalDate.of(1995, Month.APRIL, 10));
		paciente.setPacienteHab(habitacionDAO.read(1));
		pacienteDAO.create(paciente);

		Paciente paciente2 = new Paciente();
		paciente2.setNombre("Marcos");
		paciente2.setApellidos("Jimenez");
		paciente2.setDni("89256734g");
		paciente2.setCentroUsers(centroDAO.read(2));
		paciente2.setFec_nac(LocalDate.of(1990, Month.MARCH, 30));
		paciente2.setPacienteHab(habitacionDAO.read(1));
		pacienteDAO.create(paciente2);

		System.out.println("Lista de usuarios tipo pacientes:");
		List<Paciente> listaUsersp = pacienteDAO.listarPacientes();

		for (int i = 0; i < listaUsersp.size(); i++) {
			System.out.println(listaUsersp.get(i).toString());
		}

		System.out.println("Listar Pacientes habitacion 1 del centro 1: ");
		List<Paciente> pacienteshab = pacienteDAO.listarPacientesHab(habitacionDAO.read(1), centroDAO.read(1));

		for (int i = 0; i < pacienteshab.size(); i++) {
			System.out.println(pacienteshab.get(i).toString());
		}
	}
}
